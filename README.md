# SILL 4 Frontend 

#### Install

`cd` to project's dir and run `npm install`

### Vue CLI builds

#### Compiles and hot-reloads for development
```
npm run serve
```

#### Compiles and minifies for production
```
npm run build
```

### Vite builds

[Vite](https://vitejs.dev) is next Generation Frontend Tooling featuring unbundled web-development

#### Compiles and hot-reloads for development
```
npm run build
```

#### Compiles and minifies for production
```
npm run build:vite
```

### Linting

#### Lint
```
npm run lint
```

#### Lints and fixes files
```
npm run lint:fix
```

### Laravel 9.x integration

This dashboard can be integrated with **Laravel 9.x Jetstream Inertia + Vue.js** stack. [Check guide](https://github.com/vikdiesel/admin-one-laravel-dashboard) for more information.

## Browser Support

We try to make sure dashboard works well in the latest versions of all major browsers:

<img src="https://justboil.me/images/browsers-svg/chrome.svg" width="64" height="64" alt="Chrome"> <img src="https://justboil.me/images/browsers-svg/firefox.svg" width="64" height="64" alt="Firefox"> <img src="https://justboil.me/images/browsers-svg/edge.svg" width="64" height="64" alt="Edge"> <img src="https://justboil.me/images/browsers-svg/safari.svg" width="64" height="64" alt="Safari"> <img src="https://justboil.me/images/browsers-svg/opera.svg" width="64" height="64" alt="Opera">

## Reporting Issues

JustBoil's free items are limited to community support on GitHub.

The issue list is reserved exclusively for bug reports and feature requests. That means we do not accept usage questions. If you open an issue that does not conform to the requirements, it will be closed.

1. Make sure you are using the latest version of the dashboard
2. Provide steps to reproduce
3. Provide an expected behavior
4. Describe what is actually happening 
5. Platform, Browser & version as some issues may be browser specific

## Licensing

- Copyright &copy; 2019-2022 JustBoil.me (https://justboil.me)
- Licensed under MIT

## Useful Links

- [JustBoil.me](https://justboil.me)
- [Vue.js](https://vuejs.org)
- [Vue CLI](https://cli.vuejs.org)
- [Buefy](https://buefy.org)
- [Bulma](https://bulma.io)
